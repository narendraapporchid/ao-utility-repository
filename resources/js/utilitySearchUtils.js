var utilitySearchUtils = {
    onSolutionBarSearch : function(event) {
        var solutionPageUtil = window['SolutionPageUtil'];
        //var criteria = window['criteriaUtil'].createCriteria('all','Like','String',event.data.searchValue );
        
        var criteriaConfig = {
        	fields: ["employer","address1","eventTitle", "city", "state"],
        	query: event.data.searchValue
        }
        if($$('wsSearchresultsPage')) {
        	return;
        }
        var criteria = window['esCriteriaUtil'].createEsCriteria('MultiMatch', criteriaConfig);
        solutionPageUtil.openPage('com::apporchid::samples::wsSearchresultsPage', null, {microAppCriterias: {searchResultsId: criteria}}, null, true);
    },

};