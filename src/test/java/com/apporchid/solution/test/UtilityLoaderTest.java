package com.apporchid.solution.test;

import org.testng.annotations.Test;

import com.apporchid.solution.common.BaseSolutionTestApplication;
import com.apporchid.solution.utility.pipeline.builder.UtilityPipelineBuilder;
import com.apporchid.solution.utility.ui.builder.UtilitySolutionBuilder;
import com.apporchid.solution.utility.ui.builder.UtilityTablesAppBuilder;

public class UtilityLoaderTest extends BaseSolutionTestApplication {

	@Test(groups = { "solutionMain" })
	@Override
	public void setupSolution() {
		//deploy everything
		deploy(UtilityPipelineBuilder.class);
		deploy(UtilitySolutionBuilder.class);
		//deploy(CustomerTablesAppBuilder.class);
		
		//deploy only pipelines
		//deploy(SamplesPipelineBuilder.class);

		//deploy only applications
		//deploy(TableAppsBuilder.class);

		//run a pipeline 
		//runPipeline("SmallDataTablePipeline");
	}

	@Override
	protected String getSolutionPackage() {
		return "com.apporchid.solution.utility";
	}
}
