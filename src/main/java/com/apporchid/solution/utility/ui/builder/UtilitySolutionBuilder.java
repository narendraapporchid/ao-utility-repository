package com.apporchid.solution.utility.ui.builder;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.apporchid.common.JSFunction;
import com.apporchid.config.builder.BaseConfigurationBuilder;
import com.apporchid.foundation.common.ESearchType;
import com.apporchid.foundation.common.IJSFunction;
import com.apporchid.foundation.ui.ICSSConstants;
import com.apporchid.foundation.ui.config.search.ISmartSearchControlConfig;
import com.apporchid.foundation.ui.config.solution.ISolutionConfig;
import com.apporchid.foundation.ui.config.solution.ISolutionHeaderConfig;
import com.apporchid.foundation.ui.config.solution.ISolutionPageConfig;
import com.apporchid.solution.utility.constants.IUtilityPipelineConstants;
import com.apporchid.solution.utility.pipeline.builder.UtilityPipelineBuilder;
import com.apporchid.vulcanux.common.ui.config.UIContainerConfig;
import com.apporchid.vulcanux.config.builder.BaseSolutionConfigurationBuilder;
import com.apporchid.vulcanux.ui.config.button.BadgeConfig;
import com.apporchid.vulcanux.ui.config.button.BadgeDataConfig;
import com.apporchid.vulcanux.ui.config.search.SmartSearchControlConfig;
import com.apporchid.vulcanux.ui.config.search.data.SmartSearchControlDataConfig;
import com.apporchid.vulcanux.ui.config.solution.SolutionHeaderConfig;
import com.apporchid.vulcanux.ui.wx.component.EBadgeType;
import com.apporchid.vulcanux.ui.wx.data.WxMenuItemData;

@Component
public class UtilitySolutionBuilder extends BaseSolutionConfigurationBuilder implements IUtilityPipelineConstants {
	public UtilitySolutionBuilder() {
		super(DEFAULT_DOMAIN_ID, DEFAULT_SUB_DOMAIN_ID);
	}
	@Override
	protected List<Class<? extends BaseConfigurationBuilder<?>>> getDependentConfigBuilders() {
		List<Class<? extends BaseConfigurationBuilder<?>>> builders = new ArrayList<>();
		builders.add(UtilityPipelineBuilder.class);
		builders.add(UtilityTablesAppBuilder.class);
		return builders;
	}
	@Override
	protected ISolutionConfig getSolution() {
		List<ISolutionPageConfig> solutionPages = new ArrayList<>();

		ISolutionPageConfig customerSearchResultsPage = getSolutionPageConfig("customerSearchResultsPage", "customerSearchResultsPage", false, "vuxicon-controls",
				toApplicationReferences(getCustomerSearchResultsAppId()));
		solutionPages.add(customerSearchResultsPage);
		
		IJSFunction favMenuClick = new JSFunction("utilityPopupUtils", "openFavoriteApp");
		WxMenuItemData favMenu = new WxMenuItemData("solHeaderFavorites", "Favorites", "vuxicon-codepen", favMenuClick);
		UIContainerConfig additionalComponentsInHeader = getAdditionalComponentsInHeader();
		
		SmartSearchControlDataConfig dataConfig = new SmartSearchControlDataConfig.Builder()
				.pipelineId(getId(PIPELINE_ID_ES_AUTO_COMPLETE)).build();
		
		ISmartSearchControlConfig<?> searchBar = new SmartSearchControlConfig.Builder().withId("esSolheaderSearchbar")
//				.withDataConfig(dataConfig)
				.showNoRecordsMessage(true).withNoRecordsMessage("No matches were found in our system. Please refine your search and try again.")
				.withGhostText("Search customers")/*.onEvent(EVENT_GLOBAL_SEARCH, getJSFunction("utilityResultsDataviewUtils", "onCustomerSearchResultsPageSearch"))*/.enableHistory(true).enableAutoComplete(true)
				.withCssClass("utility-customer-search")/*.clearSearchOnClose(getJSFunction("utilityResultsDataviewUtils", "onCustomerSearchResultsPageSearch"))*/.build();
		ISolutionHeaderConfig solutionHeaderConfig = new SolutionHeaderConfig.Builder().enableNotifications(true).withSearchType(ESearchType.QNA_AND_ESSEARCH)
				.withCustomLogoPath(SOLUTION_LOGO).withDisplayName(SOLUTION_NAME).showOntologiesFilter(false).withSearchControlConfig(searchBar)
				.withCssClass("utility-customer-solution-header").withDefaultOntology(DEFAULT_SOLUTION_ONTOLOGY_UID)
				 /*.withDefaultSearchType(ESearchType.QNA)*/.withAddlProfileDataItem(favMenu) 
				 .withAdditionalContainer(additionalComponentsInHeader).build();

		
		  ISolutionConfig solutionConfig = createSolution(SOLUTION_ID, SOLUTION_NAME,
		  true, true, solutionPages, solutionHeaderConfig, SOLUTION_LOGO);
		  
		
		  return solutionConfig;

	}
	
	protected String[] getCustomerSearchResultsAppId() {
		return new String[] { "customerSearchResultsApp" };
	}
	
	private UIContainerConfig getAdditionalComponentsInHeader() {

		BadgeDataConfig badgeDataConfig = new BadgeDataConfig.Builder().pipelineId(getId("BadgeIconPipeline")).badgeField("badge").build();
		BadgeConfig badgeConfig = new BadgeConfig.Builder().withId("appHeaderIcon").withCssClass(ICSSConstants.CSS_SOL_HEADER_ICON)
				.withBadgeType(EBadgeType.ICON).property("width", 40).withIcon("vuxicon-heart").withDataConfig(badgeDataConfig).build();

		UIContainerConfig uicontainer = new UIContainerConfig.Builder().addComponent(badgeConfig).build();

		return uicontainer;
	}
	@Override
	protected String[] getJsFiles() {
		return new String[] { "js/utilitySearchUtils.js","js/utilityPopupUtils.js" };
	}

	@Override
	public String getSolutionId() {
		return getId(SOLUTION_ID);
	}
}
