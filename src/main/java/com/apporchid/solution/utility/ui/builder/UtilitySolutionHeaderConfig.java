package com.apporchid.solution.utility.ui.builder;

import com.apporchid.vulcanux.ui.config.solution.BaseSolutionHeaderConfig;

public class UtilitySolutionHeaderConfig extends BaseSolutionHeaderConfig{
	private static final long serialVersionUID = 1L;

	public UtilitySolutionHeaderConfig() {
		super();
	}
	
	public static final class Builder extends BaseSolutionHeaderConfig.Builder<UtilitySolutionHeaderConfig, Builder> {

		@Override
		protected UtilitySolutionHeaderConfig create() {
			return new UtilitySolutionHeaderConfig();
		}

		@Override
		protected Builder getBuilder() {
			return this;
		}

	}
}
