package com.apporchid.solution.utility.constants;

public interface IUtilityPipelineConstants extends IUtilityCommonConstants, IUtilitySolutionConstants{
	public static final String DEFAULT_ES_LOCAL_INDEX = "customer-indexes";
	public static final String DEFAULT_ES_LOCAL_CLUSTER = "cloudseer-analytics";
	public static final String AO_UTILITY_CUSTOMER_ES_INDEX="ao-utility-utility";
	public static final String PIPELINE_ID_ES_AUTO_COMPLETE = "UtilityIndexAutoCompletePipeline";
	public static final String AO_UTILITY_CUSTOMER_ES_DATA_LOAD_PIPELINE="aoUtilityCustomerESDataLoad";
	public static final String CUSTOMER_SEARCH_MSO_NAME = "CustomerSearch";
	public static final String CUSTOMER_SEARCH_MSO = DEFAULT_DOMAIN_ID + "." + DEFAULT_SUB_DOMAIN_ID + "." + CUSTOMER_SEARCH_MSO_NAME;
}
