package com.apporchid.solution.utility.constants;

public interface IUtilitySolutionConstants extends IUtilityCommonConstants {
	public static final String SOLUTION_ID = "utility";
	public static final String RESOURCE_LOCATION = "main-ui/solutions/" + SOLUTION_ID;
	public static final String SOLUTION_LOGO = RESOURCE_LOCATION + "/images/utility-logo.png";
	public static final String SOLUTION_ICON = RESOURCE_LOCATION + "/images/utility-icon.png";
	
	public static final String SOLUTION_NAME = "utility";
	// solution specific images parent folder
	public static final String SOLUTION_PATH = "/main-ui/solutions/" + SOLUTION_ID;

	// solution specific images parent folder
	public static final String IMAGES_ROOT_PATH = SOLUTION_PATH + "/images/";
	
	public static final String AO_UTILITY_DB_DATASOURCE = "utilityImplDB";
	public static final String DEFAULT_SOLUTION_ONTOLOGY_UID = "UtilityOntology";
}
