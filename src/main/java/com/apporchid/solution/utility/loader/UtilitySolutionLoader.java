package com.apporchid.solution.utility.loader;

import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.apporchid.solution.utility.ui.builder.UtilitySolutionBuilder;
import com.apporchid.vulcanux.config.loader.BaseSolutionLoader;
@Profile("!solution-dev")
@Component
@DependsOn(value = { "VuxCacheLoader" })
public class UtilitySolutionLoader extends BaseSolutionLoader<UtilitySolutionBuilder>{
	private static final String[] DEFAULT_ACCESS_ROLES = new String[] { "administrator", "demo_user" };

	@Override
	protected Class<UtilitySolutionBuilder> getSolutionBuilderType() {
		return UtilitySolutionBuilder.class;
	}

	@Override
	protected boolean isReloadOnServerStartup() {
		return false;
	}

	@Override
	protected String[] getAccessRoles() {
		return DEFAULT_ACCESS_ROLES;
	}
}
